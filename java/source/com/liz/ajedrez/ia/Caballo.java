package com.liz.ajedrez.ia;

import java.util.ArrayList;
import java.util.List;

public class Caballo extends Pieza {

    public Caballo(boolean negra, int posX, int posY) {
        super(negra, negra ? 'c' : 'C', 200, posX, posY);
    }

    public List<Posicion> calcularMovimientos(Tablero tablero) {
    	posiciones = new ArrayList<Posicion>();
        //posiciones cuadrante I.
        int i = 0, j = 0;
        i = posX + 1;
        j = posY - 2;
        auxCalcularMovimientos(i, j, tablero);
        i = posX + 2;
        j = posY - 1;
        auxCalcularMovimientos(i, j, tablero);
        //posiciones cuadrante II.
        i = posX - 1;
        j = posY - 2;
        auxCalcularMovimientos(i, j, tablero);
        i = posX - 2;
        j = posY - 1;
        auxCalcularMovimientos(i, j, tablero);
        //posiciones cuadrante III.
        i = posX - 1;
        j = posY + 2;
        auxCalcularMovimientos(i, j, tablero);
        i = posX - 2;
        j = posY + 1;
        auxCalcularMovimientos(i, j, tablero);
        //posiciones cuadrante IV.
        i = posX + 1;
        j = posY + 2;
        auxCalcularMovimientos(i, j, tablero);
        i = posX + 2;
        j = posY + 1;
        auxCalcularMovimientos(i, j, tablero);
        return posiciones;
    }

    private void auxCalcularMovimientos(int i, int j, Tablero tablero) {
        if (((0 <= i) && (i < Tablero.TAMANIO)) &&
            ((0 <= j) && (j < Tablero.TAMANIO))) {
        	if (tablero.escaques[i][j].pieza == null)
                posiciones.add(new Posicion(i, j));
            else if (negra && !tablero.escaques[i][j].pieza.negra)
                posiciones.add(new Posicion(i, j));
            else if (!negra && tablero.escaques[i][j].pieza.negra)
                posiciones.add(new Posicion(i, j));
        }
    }

}
