package com.liz.ajedrez.ia;

public class Posicion {
    public int x;
    public int y;
    
    Posicion(int x, int y) {
	this.x = x;
    this.y = y;
    }
    
    public String toString() {return "[" + x + ", " + y + "]";}
}
