package com.liz.ajedrez.ia;

import java.util.ArrayList;
import java.util.List;

public class Rey extends Pieza {
    
    public Rey(boolean negra, int posX, int posY) {
        super(negra, negra ? 'r' : 'R', 1200, posX, posY);
    }

    public List<Posicion> calcularMovimientos(Tablero tablero) {
    	posiciones = new ArrayList<Posicion>();
        int i = 0, j = 0;
        i = posX + 1;
        j = posY;
        auxCalcularMovimientos(i, j, tablero);
        i = posX + 1;
        j = posY + 1;
        auxCalcularMovimientos(i, j, tablero);
        i = posX;
        j = posY + 1;
        auxCalcularMovimientos(i, j, tablero);
        i = posX - 1;
        j = posY + 1;
        auxCalcularMovimientos(i, j, tablero);
        i = posX - 1;
        j = posY;
        auxCalcularMovimientos(i, j, tablero);
        i = posX - 1;
        j = posY - 1;
        auxCalcularMovimientos(i, j, tablero);
        i = posX;
        j = posY - 1;
        auxCalcularMovimientos(i, j, tablero);
        i = posX + 1;
        j = posY - 1;
        auxCalcularMovimientos(i, j, tablero);
        //Se debe regresar el enrroque como una posicion;
        return posiciones;
    }

    private void auxCalcularMovimientos(int i, int j, Tablero tablero) {
    	if (((0 <= i) && (i < Tablero.TAMANIO)) &&
            ((0 <= j) && (j < Tablero.TAMANIO))) {
        	if (tablero.escaques[i][j].pieza == null)
                posiciones.add(new Posicion(i, j));
            else if (negra && !tablero.escaques[i][j].pieza.negra)
                posiciones.add(new Posicion(i, j));
            else if (!negra && tablero.escaques[i][j].pieza.negra)
                posiciones.add(new Posicion(i, j));
        }
    }
    
}
