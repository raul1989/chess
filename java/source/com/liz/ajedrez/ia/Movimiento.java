package com.liz.ajedrez.ia;

public class Movimiento {
    public Pieza pieza;
    public Posicion posicion;
    public double valor;

	Movimiento(Pieza pieza, Posicion posicion, double valor) {
		this.pieza = pieza;
		this.posicion = posicion;
		this.valor = valor;
    }

    public String toString() {
            return pieza.toString() + " " + posicion.toString();
    }
}
