package com.liz.ajedrez.ia;

import java.util.List;

public class Dama extends Pieza {
    private Torre torre;
    private Alfil alfil;

    public Dama(boolean negra, int posX, int posY) {
        super(negra, negra ? 'd' : 'D', 400, posX, posY);
        this.torre = new Torre(negra, posX, posY);
        this.alfil = new Alfil(negra, posX, posY);
    }

    public List<Posicion> calcularMovimientos(Tablero tablero) {
        return unirListas(torre.calcularMovimientos(tablero),
                          alfil.calcularMovimientos(tablero));
    }

    private List<Posicion> unirListas(List<Posicion> l1, List<Posicion> l2) {
        for(Posicion posicion : l2)
            l1.add(posicion);
        return l1;
    }

    public void setPosX(int posX) {
        this.posX = posX;
        this.torre.setPosX(posX);
        this.alfil.setPosX(posX);
    }
        
    public void setPosY(int posY) {
        this.posY = posY;
        this.torre.setPosY(posY);
        this.alfil.setPosY(posY);
    }
}
