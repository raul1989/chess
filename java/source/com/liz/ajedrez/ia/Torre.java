package com.liz.ajedrez.ia;

import java.util.ArrayList;
import java.util.List;

public class Torre extends Pieza {
    public Torre(boolean negra, int posX, int posY) {
    	super(negra, negra ? 't' : 'T', 120, posX, posY);
    }

    public List<Posicion> calcularMovimientos(Tablero tablero) {
        posiciones = new ArrayList<Posicion>();
        //Movimiento en X+. //Al parecer hay que aumentar posx en 1.
        for (int i = posX + 1; i < Tablero.TAMANIO; i++) {
            if (tablero.escaques[i][posY].pieza == null)
                posiciones.add(new Posicion(i, posY));
            else if (negra && !tablero.escaques[i][posY].pieza.negra) {
                posiciones.add(new Posicion(i, posY));
                break;
            }
            else if (negra && tablero.escaques[i][posY].pieza.negra)
                break;
            else if (!negra && tablero.escaques[i][posY].pieza.negra) {
                posiciones.add(new Posicion(i, posY));
                break;
            }
            else if (!negra && !tablero.escaques[i][posY].pieza.negra)
                break;
        }
        //Movimiento en Y+.
        for (int j = posY + 1; j < Tablero.TAMANIO; j++) {
            if (tablero.escaques[posX][j].pieza == null)
                posiciones.add(new Posicion(posX, j));
            else if (negra && !tablero.escaques[posX][j].pieza.negra) {
                posiciones.add(new Posicion(posX, j));
                break;
            }
            else if (negra && tablero.escaques[posX][j].pieza.negra)
                break;
            else if (!negra && tablero.escaques[posX][j].pieza.negra) {
                posiciones.add(new Posicion(posX, j));
                break;
            }
            else if (!negra && !tablero.escaques[posX][j].pieza.negra)
                break;
        }
        //Movimiento en X-.
        for (int i = posX - 1; i >= 0; i--) {
            if (tablero.escaques[i][posY].pieza == null)
                posiciones.add(new Posicion(i, posY));
            else if (negra && !tablero.escaques[i][posY].pieza.negra) {
                posiciones.add(new Posicion(i, posY));
                break;
            }
            else if (negra && tablero.escaques[i][posY].pieza.negra)
                break;
            else if (!negra && tablero.escaques[i][posY].pieza.negra) {
                posiciones.add(new Posicion(i, posY));
                break;
            }
            else if (!negra && !tablero.escaques[i][posY].pieza.negra)
                break;
        }
        //Movimiento en Y-.
        for (int j = posY - 1; j >= 0; j--) {
            if (tablero.escaques[posX][j].pieza == null)
                posiciones.add(new Posicion(posX, j));
            else if (negra && !tablero.escaques[posX][j].pieza.negra) {
                posiciones.add(new Posicion(posX, j));
                break;
            }
            else if (negra && tablero.escaques[posX][j].pieza.negra)
                break;
            else if (!negra && tablero.escaques[posX][j].pieza.negra) {
                posiciones.add(new Posicion(posX, j));
                break;
            }
            else if (!negra && !tablero.escaques[posX][j].pieza.negra)
                break;
        }
        return posiciones;
    }
}