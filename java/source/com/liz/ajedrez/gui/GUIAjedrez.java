package com.liz.ajedrez.gui;

import com.liz.ajedrez.ia.Ajedrez;
import java.awt.FlowLayout;
import javax.swing.JFrame;

public class GUIAjedrez extends JFrame {
    private GUITablero tablero;
    private GUIBitacora bitacora;
    private GUIEstado estado;
	public static Ajedrez ajedrez;
    
    public GUIAjedrez() {
	super("Ajedrez");
	getContentPane().setLayout(new FlowLayout());
	crearAgregarComponentes();
	setResizable(false);
	setSize(850, 768);
	setLocationRelativeTo(null);
	setVisible(true);
	ajedrez = new Ajedrez();
    }
    
    private void crearAgregarComponentes() {
	estado = new GUIEstado();
	getContentPane().add(estado);
	tablero = new GUITablero();
	getContentPane().add(tablero);
	bitacora = new GUIBitacora();
	getContentPane().add(bitacora);
    }
    
    public static void main(String[] args) {
	new GUIAjedrez();
    }
}