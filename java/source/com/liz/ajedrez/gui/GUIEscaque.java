package com.liz.ajedrez.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

public class GUIEscaque extends JPanel {
    private Image imagen;
    private Color colorTemporal;
	private Color colorFondo;
    private int posX;
    private int posY;
    
    public GUIEscaque(int posX, int posY) {
	this.posX = posX;
	this.posY = posY;
	setPreferredSize(new Dimension(64, 64));	
    }
    
    public void cambiarImagen(Image imagen) {
	this.imagen = imagen;
	repaint();
    }
    
    public void cambiarColor(Color color) {
	this.colorTemporal = color;
	repaint();
    }
    
    public void pintarFondo(Color color) {
    this.colorFondo = color;
    cambiarColor(color);
    }
    
    public void reiniciarFondo() {
	cambiarColor(colorFondo);    	
    }
    
    public void paint(Graphics g) {
	g.setColor(colorTemporal);
	g.fillRect(0, 0, getWidth(), getHeight());
	g.drawImage(imagen, 0, 0, getWidth(), getHeight(), null);
    }

	public Image getImagen() {return imagen;}
	public int getPosX() {return posX;}
	public int getPosY() {return posY;}
}